//EZSocial
//Uses dk.napp.social and Android Intents to Share Items of Interest

exports.share = function(name, link) {
	if (OS_ANDROID) {
		var intent = Ti.Android.createIntent({
			action : Ti.Android.ACTION_SEND,
			type : "text/plain"
		});
		intent.putExtra(Ti.Android.EXTRA_TEXT, name + " " + link);
		intent.addCategory(Ti.Android.CATEGORY_DEFAULT);
		Ti.Android.currentActivity.startActivity(intent);
	} else if (OS_IOS) {
		var Social = require('dk.napp.social');
		if (Social.isActivityViewSupported()) {//min iOS6 required
			Social.activityView({
				text : name + " " + link,
				removeIcons : "print,copy,contact,camera"
			});
		} else {
			alert("You need iOS 6 or higher to use this feature.");
		}
	} else {
		alert("Your device does not support sharing.");
	}
};
