var Cloud = require('ti.cloud');
var type; //type of journal to filter by
var entries = [];//stored journals
var args = [];

var logoutButton = Titanium.UI.createButton({
	style : "plain",
	backgroundColor: "#EE5733",
	color: "#fff",
	font: {
		fontSize: "12dp",
		fontWeight: "bold"
	},
	height: "32dp",
	width: Ti.UI.FILL,
	bottom: "0dp",
	title : "LOGOUT"
});
$.medicalCategoryBtn.opacity = "0.5";
$.milestonesCategoryBtn.opacity = "0.5";
$.notesCategoryBtn.opacity = "0.5";

var logoutRow = Titanium.UI.createTableViewRow({
	height : Ti.UI.SIZE,
	width : Ti.UI.FILL
});
logoutRow.add(logoutButton);

//Checks to see if the login screen has to be shown
var hideLogin = function(flag){
	if(flag){
		$.loginScreen.setHeight("0dp");
		$.loginScreen.hide();
	}
	else{
		$.loginScreen.setHeight(Ti.UI.FILL);
		$.loginScreen.show();
	}
};

var hideJournal = function(flag){
	if(flag){
		$.journalScreen.setHeight("0dp");
		$.journalScreen.hide();
	}
	else{
		$.journalScreen.setHeight(Ti.UI.FILL);
		$.journalScreen.show();
	}
};
hideJournal(true);

//Retrieves journals from ACS
var getEntries = function() {
	if (Ti.App.Properties.getString("userLog") == null) {
		Ti.API.info("Not logged in.");
	} else {
		entries = [];
		var userLog = Ti.App.Properties.getString("userLog");
		Cloud.Objects.query({
			classname : 'journals',
			where : {
				user_id : userLog
			}
		}, function(e) {
			if (e.success) {
				var data = [];
				for (var i = 0; i < e.journals.length; ++i) {
					var journalData = {
						title : e.journals[i].title,
						text : e.journals[i].text,
						date : e.journals[i].date,
						type : e.journals[i].type,
						id : e.journals[i].id,
						index : i
					};
					if(journalData.type === 'medical'){
						journalData.color = "#f19333";
					}else if(journalData.type === 'milestones'){
						journalData.color = "#8db74b";
					}else if(journalData.type === 'notes'){
						journalData.color = "#68b0bc";
					}
					entries.push(journalData);//stores them 					
					var journal_row = Alloy.createController("journal_row", journalData).getView();
					data.push(journal_row);//initial table population
				}
				data.push(logoutRow);
				$.entriesTable.setData(data);
				 $.entriesTable.height = Ti.UI.FILL;
			} else {
				alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
			}
		});
	}
};

//filters out specific entries
var filterEntries = function(type){
	var filteredData = [];
	for(var i = 0; i < entries.length; i++){
		if(entries[i].type === type){
			var journal_row = Alloy.createController('journal_row', entries[i]).getView();
			filteredData.push(journal_row);
		}
	}
	filteredData.push(logoutRow);
	$.entriesTable.setData(filteredData);
};

$.username.addEventListener('blur', function(e) {
	$.username.value = $.username.value.toLowerCase();
});

//handles user login
$.loginButton.addEventListener("click", function(e) {
	$.username.blur();
	$.password.blur();
	Ti.API.info("Trying to login with credentials " + $.username.value + ", " + $.password.value);
	Cloud.Users.login({
		login : $.username.value.toLowerCase(),
		password : $.password.value
	}, function(e) {
		if (e.success) {
			Ti.API.info("Successful Login");
			Ti.API.info("Inspecting Object: e.users[0]:" + e.users[0].id);
			Ti.App.Properties.setString("userLog", e.users[0].id);
			Ti.App.Properties.setString("user", $.username.value);
			Ti.App.Properties.setString("pass", $.password.value);
			hideJournal(false);
			hideLogin(true);
			getEntries();
		} else {
			alert(((e.error && e.message) || JSON.stringify(e)));
		}
	});
});

//opens journal creation screen
$.newEntryButton.addEventListener('click', function(e) {
	Alloy.createController('journal_create').getView().open();
});

//change button color on click
$.entryCategory.addEventListener('click', function(e) {
	
	if(e.source.opacity === "0.5"){
		//turn all buttons off
		for (var i = 0; i < this.children.length; i++) {
			this.children[i].children[0].opacity = "0.5";
		}
		//turn clicked button on
		e.source.opacity = "1.0";
	}else{
		e.source.opacity = "0.5";
	}
});

//filter listeners
$.medicalCategoryBtn.addEventListener('click', function(e) {
	if($.medicalCategoryBtn.opacity === "0.5"){
		Ti.API.info("Filter by Medical Notes");
		type = "medical";
		filterEntries(type);
	}else{
		var rows = [];
		for(var i = 0; i < entries.length; i++){
			var journal_row = Alloy.createController('journal_row', entries[i]).getView();
			rows.push(journal_row);
		}
		rows.push(logoutRow);
		$.entriesTable.setData(rows);
	}
});

$.milestonesCategoryBtn.addEventListener('click', function(e) {
	if($.milestonesCategoryBtn.opacity === "0.5"){
		Ti.API.info("Filter by Milestones Notes");
		type = "milestones";
		filterEntries(type);
	}else{
		var rows = [];
		for(var i = 0; i < entries.length; i++){
			var journal_row = Alloy.createController('journal_row', entries[i]).getView();
			rows.push(journal_row);
		}
		rows.push(logoutRow);
		$.entriesTable.setData(rows);
	}
});

$.notesCategoryBtn.addEventListener('click', function(e) {
	if($.notesCategoryBtn.opacity === "0.5"){
		Ti.API.info("Filter by General Notes");
		type = "notes";
		filterEntries(type);
	}else{
		var rows = [];
		for(var i = 0; i < entries.length; i++){
			var journal_row = Alloy.createController('journal_row', entries[i]).getView();
			rows.push(journal_row);
		}
		rows.push(logoutRow);
		$.entriesTable.setData(rows);
	}
});

//Logs a user out
logoutButton.addEventListener('click', function(e) {
	Cloud.Users.logout(function(e) {
		if (e.success) {
			alert("You have been logged out!");
			Ti.API.info('Success: Logged Out');
			Ti.App.Properties.setString("userLog", null);
			Ti.App.Properties.setString("user", null);
			Ti.App.Properties.setString("pass", null);
			$.password.setValue("");
			$.username.setValue("");
			hideJournal(true);
			hideLogin(false);
		} else {
			alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});
});

//Registration screen
$.registerButton.addEventListener('click', function(e) {
	Alloy.createController('createUser').getView().open();
});

//Listener for when a new journal has been added
Ti.App.addEventListener('journalCreated', function(){
	getEntries();
});

// Ti.App.addEventListener('hideKeyboard',function(e){
// 	// $.username.enable = false;
// 	// $.username.enable = true;
// 	// $.password.enable = false;
// 	// $.password.enable = true;
// 	$.username.blur();
// 	$.password.blur();
// });
//Logs the user back in if they didn't log out during the last use of the app
if(Ti.App.Properties.getString("userLog") != null){
	if(Ti.App.Properties.getString("user") == null)
		alert('Old Version\n Clear your data for this app and delete it. Reinstall the app and it will work.');
	$.username.setValue(Ti.App.Properties.getString("user"));
	$.password.setValue(Ti.App.Properties.getString("pass"));
	$.loginButton.fireEvent('click');
}

