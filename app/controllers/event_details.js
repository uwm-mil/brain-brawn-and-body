//add this controller to the back history
Alloy.Globals.backHistory.push($.win);

var args = arguments[0] || {};
var ezsocial = require('ezsocial');

// handle expanded row
$.weekdayLabel.text = args.weekday;
$.monthLabel.text = args.month;
$.dateLabel.text = args.date;

$.startTimeLabel.text = args.startTime;
if($.startTimeLabel.text === "Event time"){
	$.startTimeLabel.opacity = ".5";
}
$.endTimeLabel.text = args.endTime;
if($.endTimeLabel.text === "not given"){
	$.endTimeLabel.opacity = ".5";
}

$.titleLabel.text = args.title;

$.detailLabel.text = args.detail;

// handle bottom three additional "rows"
$.locationPlaceLabel.text = args.location;

$.shareButton.addEventListener("click", function(e) {
	ezsocial.share(args.title, "https://www.facebook.com/events/" + args.id);
});
