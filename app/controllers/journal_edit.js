var type = "notSelected";
var Cloud = require('ti.cloud');
var picker;
var dateToExport;
var args = arguments[0] || {};
var animation = require('alloy/animation');
//add this controller to the back history
Alloy.Globals.backHistory.push($.journal_edit);

$.medicalCategoryBtn.opacity = "0.5";
$.milestonesCategoryBtn.opacity = "0.5";
$.notesCategoryBtn.opacity = "0.5";

var pickerView = Ti.UI.createView({
    width:Ti.UI.FILL,
    height:Ti.UI.SIZE,
    layout : "vertical"
});
 
var picker = Ti.UI.createPicker({
    type:Ti.UI.PICKER_TYPE_DATE,
    bottom:"0dp",
  	minDate:new Date(2009,0,1),
  	maxDate:new Date(2014,11,31),
  	value:new Date(),
  	height : Ti.UI.SIZE
});
var button = Ti.UI.createButton({
    title: "Set Date",
	height: "32dp",
	width: Ti.UI.FILL,
	top: "4dp",
	right: "4dp",
	bottom: "4dp",
	left: "4dp",
    style: "plain",
	backgroundColor: "#EE5733",
	color: "#fff",
	font: {
		fontSize: "12dp",
		fontWeight: "bold"
	}
});
pickerView.add(button);
pickerView.add(picker);
$.pickerWrapper.add(pickerView);
button.addEventListener("click",function () {
	// set value 
	var pickerdate = picker.value;
    var day = pickerdate.getDate();
    var month = pickerdate.getMonth() + 1;
    if(month < 10)
    	month = "0" + month;
    var year = pickerdate.getFullYear();
    var newdate = year + "-" + month + "-" + day;
    $.entryDate.text = moment(newdate).format('MMMM Do YYYY');
    dateToExport = moment(newdate).format('YYYYMMDD');
	// animate off
	$.pickerWrapper.animate({
		bottom : "-260dp",
		duration : 250,
		curve : Titanium.UI.ANIMATION_CURVE_EASE_IN_OUT
	});
});

$.entryDateWrapper.addEventListener('click', function(e) {
	// scroll height after adding picker
	$.pickerWrapper.bottom = "-260dp";
	
	$.pickerWrapper.animate({
		bottom : "0dp",
		duration : 250,
		curve : Titanium.UI.ANIMATION_CURVE_EASE_IN_OUT
	});

});

/***********************/
// fill in the blanks with passed in datata
Ti.API.info("content : " + args.content + " " + args.id);
Ti.API.info("color : " + args.typeColor);
$.entryTitle.setValue(args.title);
$.entryContent.setValue(args.content);
Ti.API.info("And teh date is..." + args.date);
var newdate = args.date.slice(0,4) + "-" + args.date.slice(4,6) + "-" + args.date.slice(6,8);
Ti.API.info("And teh date is now..." + newdate);
$.entryDate.text = moment(newdate).format('MMMM Do YYYY');

// set button selection
if(args.typeColor == "#f19333"){	// medical, orange
	$.medicalCategoryBtn.opacity = "1.0";
	type = "medical";
}else if (args.typeColor == "#8db74b"){	// milestone, green
	$.milestonesCategoryBtn.opacity = "1.0";
	type = "milestones";
}else{	//#68b0bc" - notes, blue
	$.notesCategoryBtn.opacity = "1.0";
	type = "notes";
}

/*********************** listeners ************************/
$.entryDateWrapper.addEventListener('click', function() {
	//date picker should popup here
	//Format YYYYMMDD
});

$.entryCategory.addEventListener('click', function(e) {
	//turn all buttons off
	for (var i = 0; i < this.children.length; i++) {
		this.children[i].children[0].opacity = "0.5";
	}
	//turn clicked button on
	e.source.opacity = "1.0";
});

//Listeners to assign journal type
$.medicalCategoryBtn.addEventListener('click', function(e) {
	Ti.API.info("Medical Note");
	type = "medical";
});

$.milestonesCategoryBtn.addEventListener('click', function(e) {
	Ti.API.info("Milestones Note");
	type = "milestones";
});

$.notesCategoryBtn.addEventListener('click', function(e) {
	Ti.API.info("General Note");
	type = "notes";
});

//updates journal entry
$.submitEntryBtn.addEventListener('click', function(e) {
	if(type !== "notSelected"){
	Cloud.Objects.update({
		classname : 'journals',
		id : args.id,
		fields : {
			type: type,
			text : $.entryContent.value,
			//Date Format: YYYYMMDD
			date : dateToExport,
			title : $.entryTitle.value
		}
	}, function(e) {
		if (e.success) {
			Ti.API.info("Journal Updated");
			Alloy.Globals.backHistory.pop().close();
			Titanium.App.fireEvent('journalCreated');
		} else {
			alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});
	}else{
		alert("Please select the type of entry this will be.");
	}
});

$.deleteEntryBtn.addEventListener('click', function(e) {
	Cloud.Objects.remove({
    	classname: 'journals',
    	id: args.id,
	}, function (e) {
    	if (e.success) {
       		alert('Journal Deleted');
       		Alloy.Globals.backHistory.pop().close();
			Titanium.App.fireEvent('journalCreated');
    	} else {
        	alert('Error:\n' +
            ((e.error && e.message) || JSON.stringify(e)));
    	}
	});
});