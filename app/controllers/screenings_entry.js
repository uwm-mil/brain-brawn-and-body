//VARIABLES
var type = "notSelected";
var Cloud = require('ti.cloud');
var picker;
var dateToExport;
var animation = require('alloy/animation');
var moment = require('alloy/moment');
var args = arguments[0] || {};
var currentTime = new Date();
var year = currentTime.getFullYear();

//EVENTS
$.submitEntryBtn.addEventListener('click', function(e) {
	Cloud.Objects.create({
		classname : 'screenings',
		fields : {
			ageGroup : args.ageGroup,
			name : args.screening,
			dateCompleted : moment()
		}
	}, function(e) {
		if (e.success) {
			Ti.API.info("Completed screening created");
			// refresh the Completed Entries
			var journalContent = args.screening + ": \n";
			if($.data1.value > 0) {
				journalContent += $.dataText1.text + " ";
				journalContent += $.data1.value + " \n";
			}
			if($.data2.value > 0) {
				journalContent += $.dataText2.text + " ";
				journalContent += $.data2.value + " \n";
			}
			if($.data3.value > 0) {
				journalContent += $.dataText3.text + " ";
				journalContent += $.data3.value;
			}
			Alloy.createController('journal_create', {
				content : journalContent,
				screeningTitle: args.screening,
				dateCompleted : moment()
			}).getView().open();
		} else {
			// alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
			errorFlag = true;
		}
	});
});

//INIT
Ti.API.info("Inspecting Object: " + args);
for (var thing in args) {
	Ti.API.info("args." + thing + " = " + args[thing]);
}

//marks the screening as complete
Alloy.Globals.backHistory.push($.screenings_entry);
$.screeningType.text = "Please enter the following information for your " + args.screening.toLowerCase() + " entry:";

if (args.screening == "Blood pressure") {
	$.dataText1.text = "Systolic (mm, Top Number):";
	$.dataText2.text = "Diastolic (Hg, Bottom Number):";
	$.contentWrapper.remove($.data3);
	$.contentWrapper.remove($.dataText3);
} else if (args.screening == "Cholesterol test") {
	$.dataText1.text = "Total Cholesterol (mg/dL):";
	$.dataText2.text = "HDL Cholesterol (mg/dL):";
	$.dataText3.text = "LDL Cholesterol (mg/dL):";
} else if (args.screening == "Diabetic blood sugar test") {
	$.dataText1.text = "Blood Sugar (mg/dL):";
	$.dataText2.text = "A1C (%):";
	$.contentWrapper.remove($.data3);
	$.contentWrapper.remove($.dataText3);
} else if (args.screening == "Body mass index") {
	$.dataText1.text = "Calculated BMI:";
	$.contentWrapper.remove($.data2);
	$.contentWrapper.remove($.dataText2);
	$.contentWrapper.remove($.data3);
	$.contentWrapper.remove($.dataText3);
}
