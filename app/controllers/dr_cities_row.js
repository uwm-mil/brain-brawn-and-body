var args = arguments[0] || {};

$.name.text = args.name;
$.row.filter = args.name;

$.row.addEventListener("click", function(e) {
	var alert = Ti.UI.createAlertDialog({
		buttonNames : ['Yes', 'No'],
		message : 'Would you like to save this location for future use?',
		title : args.name + ", " + args.stateName
	});
	alert.addEventListener('click', function(e) {
		if (e.index == "0") {
			Ti.App.Properties.setString("cityId", args.cityId);
			Ti.App.Properties.setString("stateId", args.stateId);
		}
		Alloy.createController("dr_speciality", {
			cityId : args.cityId,
			stateId : args.stateID,
			cityName: args.name
		}).getView().open();
	});
	alert.show();
});