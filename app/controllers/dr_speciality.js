//add this controller to the back history
Alloy.Globals.backHistory.push($.dr_speciality);

var args = arguments[0] || {};

/*
var fsCache = require('fscache');

var listSpecialities = function(arr) {
	var data = [];
	for (var i = 0; i < arr.result.length; i++) {
		var speciality = arr.result[i];
		Ti.API.info("Inspecting Object: speciality:" + speciality);
		for (var thing in speciality) {
		  Ti.API.info("speciality." + thing + " = " + speciality[thing]);
		}
		data.push(Alloy.createController("dr_speciality_row", {
			specialityId : speciality.id,
			stateId: args.stateID,
			name : speciality.name,
			stateName: args.name,
			cityId: args.cityId,
			cityName: args.cityName
		}).getView());
	}
	$.tv.setData(data);
};

fsCache.process("https://api.doctoralia.com/v1/US/specialities?apiKey=8da25879", 86400, "specialities", listSpecialities);
*/

//$.tv.search = $.tableSearch;

var url = "https://api.doctoralia.com/v1/US/specialities?apiKey=8da25879";
var client = Ti.Network.createHTTPClient({
		onload : function(e) {			
			var result = JSON.parse(this.responseText).result;
			var data = [];
			for (var i = 0; i < result.length; i++) {
				var speciality = result[i];
				data.push(Alloy.createController("dr_speciality_row", {
					specialityId : speciality.id,
					stateId: args.stateID,
					name : speciality.name,
					stateName: args.name,
					cityId: args.cityId,
					cityName: args.cityName
				}).getView());
			}
			$.tv.setData(data);
			
		},
		onerror : function(e) {
			Ti.API.debug(e.error);
			alert('error');
		},
		timeout : 5000  // in milliseconds
	});
client.open("GET", url);
client.send();