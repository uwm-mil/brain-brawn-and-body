var args = arguments[0] || {};

var init = function(){
	$.question.text = args.question;
	$.ansA.children[1].text = args.A;
	$.ansB.children[1].text = args.B;
	$.ansC.children[1].text = args.C;
};

$.answers.addEventListener('click', function(e){
	Ti.API.info($.answers.data[0].rows[e.index].children[0].title);

	//reset button colors
	for(var i = 0; i < 3; i++){
		this.data[0].rows[i].children[0].backgroundColor = "#e6ded2";
		this.data[0].rows[i].children[0].color = "#3c2316";
	}
	//set active button
	this.data[0].rows[e.index].children[0].backgroundColor = "#ee5733";
	this.data[0].rows[e.index].children[0].color = "#fff";

	//Scoring
	//the points per answer are variable and coming from the cloud
	//the event "addScore" throws the value into this question's position
	//in the array which holds the individual values
	switch (e.index) {
		case 0:
			Titanium.App.fireEvent("addScore", {score: args.Apts});
			break;
		case 1:
			Titanium.App.fireEvent("addScore", {score: args.Bpts});
			break;
		case 2:
			Titanium.App.fireEvent("addScore", {score: args.Cpts});
			break;
		default:
			Titanium.App.fireEvent("addScore", {score: null});
   			break;
	}

});

init();