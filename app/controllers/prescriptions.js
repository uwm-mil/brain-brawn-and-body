//add this controller to the back history
Alloy.Globals.backHistory.push($.prescriptions);

var type;

$.pInfo.visible = false;

var getPrice = function(u) {
	var xhr = Ti.Network.createHTTPClient();
	xhr.onload = function(e) {
		try {
			Ti.API.info(this.responseText);
			var arr = JSON.parse(this.responseText);
			if (arr.message) {
				alert("Error: " + arr.message);
				//alert("ERROR");
			} else {
				$.name.text = arr[0].display;
				$.doseAndForm.text = arr[0].dosage + " " + arr[0].form;
				$.brand.text = arr[0].manufacturer;
				var price;
				if (arr[0].price[0].substring(1, 2) == ".") {
					Ti.API.info("Less than $10");
					price = "$" + arr[0].price[0].substring(0, 4);
					if(price.substring(4,5) == "") {
						price = price + "0";
					}
				} else if (arr[0].price[0].substring(2, 3) == ".") {
					Ti.API.info("Between $10-$99");
					price = "$" + arr[0].price[0].substring(0, 4);
					if(price.substring(5,6) == "") {
						price = price + "0";
					}
				} else if (arr[0].price[0].substring(3, 4) == ".") {
					Ti.API.info("Between $100-999");
					price = "$" + arr[0].price[0].substring(0, 6);
					if(price.substring(6,7) == "") {
						price = price + "0";
					}
				} else {
					price = "$" + arr[0].price[0];
				}
				$.price.text = price + " for " + arr[0].quantity;
				$.grxOpen.addEventListener("click", function(e) {
					Alloy.createController("web_view", {
						link : arr[0].mobileUrl
					}).getView().open();
				});
			}
		} catch(e) {
			Ti.API.info(e);
		}
	};
	xhr.open("GET", u);
	xhr.send();
};

$.generic.addEventListener("click", function(e) {
	type = "generic";
	Ti.API.info("Searching for Generic Pricing");
	$.brand.opacity = "0.5";
	$.generic.opacity = "1.0";
});

$.brand.addEventListener("click", function(e) {
	type = "brand";
	Ti.API.info("Searching for Brand Pricing");
	$.generic.opacity = "0.5";
	$.brand.opacity = "1.0";
});

$.check.addEventListener("click", function(e) {
	var lastLetter = $.dosage.value.substring($.dosage.value.length - 1, $.dosage.value.length);
	$.drugName.blur();
	$.dosage.blur();
	if (type !== "brand" && type !== "generic") {
		alert("Please select generic or brand to search prices.");
	} else if ($.drugName.value == "") {
		alert("Please enter a drug name");
	} else if (/^[a-zA-Z]+$/.test(lastLetter) == 0 && $.dosage.value !== "") {
		alert("Your dosage should end with a unit (mg, iu, ml, etc.).");
	} else if ($.drugName.value !== "" && $.dosage.value == "") {
		var url = "https://api.carepass.com/good-rx-api/drugprices/low?name=" + $.drugName.value + "&manufacturer=" + type + "&apikey=56pddv3jc279cr57j4h5muy6";
		Ti.API.info(url);
		getPrice(url);
		$.pInfo.visible = true;

	} else {
		var url = "https://api.carepass.com/good-rx-api/drugprices/low?name=" + $.drugName.value + "&dosage=" + $.dosage.value + "&manufacturer=" + type + "&apikey=56pddv3jc279cr57j4h5muy6";
		Ti.API.info(url);
		getPrice(url);
		$.pInfo.visible = true;
	}
});
