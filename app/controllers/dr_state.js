//add this controller to the back history
Alloy.Globals.backHistory.push($.dr_state);

/*
var fsCache = require('fscache');

var listStates = function(arr) {
	var data = [];
	for (var i = 0; i < arr.result.length; i++) {
		var doc = arr.result[i];
		Ti.API.info("Inspecting Object: doc:" + doc);
		for (var thing in doc) {
		  Ti.API.info("doc." + thing + " = " + doc[thing]);
		}
		data.push(Alloy.createController("dr_state_row", {
			id : doc.id,
			name : doc.name
		}).getView());
	}
	$.tv.setData(data);
};

fsCache.process("https://api.doctoralia.com/v1/US/provinces?apiKey=8da25879", 86400, "finder_state", listStates);
*/

//$.tv.search = $.tableSearch;

var url = "https://api.doctoralia.com/v1/US/provinces?apiKey=8da25879";
var client = Ti.Network.createHTTPClient({
		onload : function(e) {			
			var result = JSON.parse(this.responseText).result;
			var data = [];
			for (var i = 0; i < result.length; i++) {
				var doc = result[i];
				data.push(Alloy.createController("dr_state_row", {
					id : doc.id,
					name : doc.name
				}).getView());
			}
			$.tv.setData(data);
			
		},
		onerror : function(e) {
			Ti.API.debug(e.error);
			alert('error');
		},
		timeout : 5000  // in milliseconds
	});
client.open("GET", url);
client.send();


