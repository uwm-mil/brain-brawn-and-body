$.scrollableView.showPagingControl = false;
var platform;
if (Titanium.Platform.name == 'iPhone OS')
	platform = "ios";
else
	platform = "android";
/*
 * scroll to window
 * update activeNavItemIndicator
 */
$.mainNavButtons.addEventListener('click', function(e) {
	$.scrollableView.scrollToView(parseInt(e.source.parent.index));

	hideNavItemIndicators();
	e.source.parent.children[1].setVisible(true);
});

/*
 * show hide activeNavItemIndicator
 */
function hideNavItemIndicators() {
	for (var i = 0; i < $.mainNavButtons.children.length; i++) {
		$.mainNavButtons.children[i].children[1].setVisible(false);
	}
}

/*
 * hide all activeNavItemIndicators
 * show the first activeNavItemIndicator
 */
(function() {
	hideNavItemIndicators();
	$.mainNavButtons.children[0].children[1].setVisible(true);
})();

/*
* update main nav when scrollableView is swiped to a different window
*/
// $.scrollableView.addEventListener('scrollEnd', function(e){
// if(e.source.id === "scrollableView"){
// hideNavItemIndicators();
// $.mainNavButtons.children[e.currentPage].children[1].show();
//
// /*
// $.mainNavButtons.animate({
// left: -20 * e.currentPage,
// duration: 125,
// curve: Titanium.UI.ANIMATION_CURVE_EASE_IN_OUT
// });
// */
// } else {
// return;
// }
// });

$.scrollableView.addEventListener('scrollEnd', function(e) {
	hideNavItemIndicators();
	// Ti.App.fireEvent('hideKeyboard');
	$.mainNavButtons.children[$.scrollableView.currentPage].children[1].setVisible(true);
	if (platform == "android") {
		Ti.UI.Android.hideSoftKeyboard();
	}
});