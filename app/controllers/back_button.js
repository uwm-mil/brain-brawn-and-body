var closeIt = function(){
    // close your current window
    if (Ti.Platform.osname == 'android'){
		Ti.UI.currentWindow.removeEventListener('android:back',closeIt);
		Ti.UI.Android.hideSoftKeyboard();
	}
	Alloy.Globals.backHistory.pop().close();
};
if (Ti.Platform.osname == 'android'){
	//add event listen and function for Android back button (hardware)
	Ti.UI.currentWindow.addEventListener('android:back',closeIt);
}
//add event listen and function for Android back button (software)
$.backButton.addEventListener('click', function(){
	if (Ti.Platform.osname == 'android'){
		Ti.UI.currentWindow.removeEventListener('android:back', closeIt);
	}
	Alloy.Globals.backHistory.pop().close();
});