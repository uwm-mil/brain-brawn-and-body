//add this controller to the back history
Alloy.Globals.backHistory.push($.screenings);

/*
 * set all buttons to 0.5 opacity
 * set first button to 1.0 opacity
 */
(function(){
	lowerButtonOpacity();
	$.buttonWrapper.children[0].children[0].opacity = "1.0";
})();



/*
 * scroll to window
 */
$.buttonWrapper.addEventListener('click', function(e){
	$.scrollableView.scrollToView(parseInt(e.source.index));
	updateButtonOpacity(e);
});



/*
 * update nav when scrollableView is swiped to a different window
 */
$.scrollableView.addEventListener('scrollend', function(e){
		lowerButtonOpacity();
		$.buttonWrapper.children[$.scrollableView.currentPage].children[0].opacity = "1";
});



/*
 * update button opacity
 */
function updateButtonOpacity(e){
	lowerButtonOpacity();
	e.source.opacity = "1";
}



/*
 * set button opacity to 0.5
 */
function lowerButtonOpacity(){
	for(var i = 0; i < $.buttonWrapper.children.length; i++){
		$.buttonWrapper.children[i].children[0].opacity = "0.5";
	}	
}
