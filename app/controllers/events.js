var moment = require('alloy/moment');
var args = arguments[0] || {};

var formatDate = function() {
	var date = new Date();
	// var datestr = d.getMonth()+'/'+d.getDate()+'/'+d.getFullYear();
	var datestr = "";
	var min = date.getMinutes();
	if (date.getMinutes() < 10)
		min = "0" + date.getMinutes();
	if (date.getHours() >= 12) {

		datestr += ' ' + (date.getHours() == 12 ? date.getHours() : date.getHours() - 12) + ':' + min + ' PM';
	} else {
		datestr += ' ' + date.getHours() + ':' + min + ' AM';
	}
	return datestr;
};

//Gets BBB's Facebook page event data
var grabEvents = function() {
	//var url = "https://graph.facebook.com/101616856688/events?fields=id,name,start_time,end_time,location,description&since=now&access_token=1424293544482387|e39cd3d45c8cfd6d45628ab30c736c61";
	var url = "https://graph.facebook.com/278205672305701/events?fields=id,name,start_time,end_time,location,description&since=now&access_token=1424293544482387|e39cd3d45c8cfd6d45628ab30c736c61";

	var xhr = Ti.Network.createHTTPClient({
		// function called when the response data is available
		onload : function(e) {
			//Ti.API.info("Received text: " + this.responseText);
			var data = JSON.parse(this.responseText);
			storeEvents(data.data);
		},
		// function called when an error occurs, including a timeout
		onerror : function(e) {
			Ti.API.info(e.error);
			alert('Failed to grab Happenings. Please check your internet connection.');
		},
		timeout : 5000 // in milliseconds
	});
	//To get over the HTTP error
	xhr.setRequestHeader("User-Agent", "Appcelerator Titanium/" + Ti.version + " (" + Ti.Platform.osname + "/" + Ti.Platform.version + "; " + Ti.Platform.name + "; " + Ti.Locale.currentLocale + ";)");
	// Prepare the connection.
	xhr.open("GET", url);
	// Send the request.
	xhr.send();
};

var storeEvents = function(data) {
	args = [];
	Ti.API.info(data.length);
	/*
	 The events come in ordered from farthest out to
	 closest to the current date. This loop stores them
	 in the reverse of that order (closest ot farthest)
	 because then they are displayed that way to the user.
	 */
	for (var i = data.length - 1; i >= 0; i--) {
		var startTime = moment(data[i].start_time), endTime = moment(data[i].end_time);
		data[i].startTime = startTime.format("h:mmA");
		data[i].endTime = endTime.format("h:mmA");
		args.push(data[i]);
		/*Ti.API.info("Inspecting Object: " + data[i]);
		 for (var thing in data[i]) {
		 Ti.API.info("data[i]." + thing + " = " + data[i][thing]);
		 }*/
	}
	Ti.API.info(args.length);
	if (args.length > 0) {
		$.parentWindow.remove($.noEventsMessage);
		setRows();
	} else {
		Ti.API.info("No Facebook Events!");
		$.eventsTable.hide();
		$.noEventsMessage.show();
	}
};

// this is ready to take in data and get rid of foobar
var setRows = function() {
	var tableData = [];
	for (var i = 0; i < args.length; i++) {
		Ti.API.info("Inspecting Object: " + args[i]);
			  for (var thing in args[i]) {
				  Ti.API.info("args[i]." + thing + " = " + args[i][thing]);
			  }
		var day = moment(args[i].start_time, "YYYY-MM-DD");
		var row = Alloy.createController("events_row", {

			weekday : day.format("ddd") || "MON",
			month : day.format("MMM") || "JUL",
			date : day.format("D") || "28",

			startTime : args[i].startTime || "Event time",
			endTime : args[i].endTime || "not given",

			title : args[i].name || "Party in the Park",
			detail : args[i].description || "Duis posuere fermentum nunc quis posuere. Pellentesque lectus augue, faucibus ac tempor ac, commodo non arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nam sed lectus augue. Sed ut elementum enim. Aenean in turpis sodales, ornare velit sed, viverra urna. Donec dictum ante in magna consequat tristique. Nullam semper dictum ullamcorper. Duis massa justo, vestibulum ac nisl et, fringilla porttitor diam. Integer vel ipsum consectetur enim commodo tempor quis ut nunc. Etiam sit amet semper purus, id luctus lectus. Phasellus bibendum mattis urna, ut semper diam ultricies ut.",
			location : args[i].location,
			id : args[i].id

		}).getView();
		tableData.push(row);
	}
	$.eventsTable.setData(tableData);
};

grabEvents();