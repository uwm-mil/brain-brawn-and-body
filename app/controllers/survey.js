var Cloud = require('ti.cloud'), views = [], scoreBook, canGrade = false;

var init = function() {
	//clear views
	views = [];
	//add this controller to the back history
	Alloy.Globals.backHistory.push($.survey);
	$.nextBtn.title = "Next";
	getSurvey();

	//Used to change the title of the Next button to finish
	//in time for the user to be on the final question
	Ti.App.addEventListener("finish", function() {
		$.nextBtn.title = "Finish";
	});

	//resets the quiz from the results page
	Ti.App.addEventListener("reset", function() {
		Alloy.Globals.backHistory.pop().close();
		Alloy.createController("survey").getView().open();
		$.surveyViews.setCurrentPage(0);
	});

	//Navigate one view backwards
	$.prevBtn.addEventListener('click', function() {
		$.nextBtn.title = "Next";

		//Navigate back only if the current view is not the first view
		if ($.surveyViews.getCurrentPage() !== 0) {
			$.surveyViews.scrollToView($.surveyViews.getCurrentPage() - 1);
		}

	});

	//Navigate one view forward
	$.nextBtn.addEventListener('click', function() {

		//Checks to see if it has to be graded
		if ($.nextBtn.title == "Finish") {
			gradeQuiz();
		}

		//Navigate forward only if the current view is not the final view
		if ($.surveyViews.getCurrentPage() + 1 !== views.length) {
			$.nextBtn.title = "Next";
			$.surveyViews.scrollToView($.surveyViews.getCurrentPage() + 1);
		}

	});
	
	$.surveyViews.addEventListener("scrollEnd", function() {
		Ti.API.info("scrollEnd: " + $.surveyViews.getCurrentPage());
		if ($.surveyViews.getCurrentPage() != views.length - 1) {
			$.nextBtn.title = "Next";
			canGrade = false;
		}
		//enables grading if you swipe for navigation
		if (canGrade) {
			gradeQuiz();
		}
		//Changes the Next to Final when the last View appears so the
		//quiz can be graded
		if ($.surveyViews.getCurrentPage() == views.length - 1) {
			Titanium.App.fireEvent("finish");
			canGrade = true;
		}
		$.breadcrumb.text = $.surveyViews.getCurrentPage() + 1 + " of " + views.length;
	});

	//Accurately updates the x of total at the bottom once the
	//scroll has finished
	//Throws the score into our array of scores
	Titanium.App.addEventListener("addScore", function(e) {
		var index = $.surveyViews.getCurrentPage();
		var score = e.score;
		scoreBook[index] = score;
		Ti.API.info(scoreBook);
	});
};

var getSurvey = function() {
	Cloud.Objects.query({
		classname : 'survey_question',
		page : 1,
		per_page : 50
	}, function(e) {
		if (e.success) {
			Ti.API.info(e.survey_question.length);
			var data = e.survey_question;
			for (var i = 0; i < data.length; i++) {

				Ti.API.info("Inspecting Object: " + data[i]);
				for (var thing in data[i]) {
					Ti.API.info("data[" + i + "]." + thing + " = " + data[i][thing]);
				}

				var view = Alloy.createController("survey_row", {
					question : data[i].Question,
					A : data[i].A,
					B : data[i].B,
					C : data[i].C,
					Apts : data[i].Apts,
					Bpts : data[i].Bpts,
					Cpts : data[i].Cpts
				}).getView();

				views.push(view);
			}
			Ti.API.info("The quiz has: " + views.length + " questions.");

			//the array which holds the individual questions scores
			scoreBook = new Array(views.length);

			//the "x of total" notation at the bottom of the screen
			$.breadcrumb.text = ($.surveyViews.getCurrentPage() + 1) + " of " + views.length;
			$.surveyViews.views = views;
		} else {
			alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});
};

//Tallys the answers then gives a response based off the total
var gradeQuiz = function() {
	var tot = 0;
	var head = "";
	var answer = "";

	//Add the answers together
	//If a value is null, the user missed the question and will be
	//prompted to answer the one they missed
	for (var i = 0; i < scoreBook.length; i++) {
		Ti.API.info(scoreBook[i]);
		if (!isNaN(scoreBook[i])) {
			tot += scoreBook[i];
		} else {
			alert("Make sure you answered all the questions. \nHint: You missed question " + (i + 1));
			return;
		}
	}

	//Assigns the appropriate response
	if (tot >= 35) {
		head = "Sugar Burner";
		answer = "In the Metabolic Effect way of thinking, there are three burner heads and they lie on a continuum. On one side there are the sugar burners who tend to be insulin dominant resulting in a  metabolic preference to run off of sugar  from the food they eat rather than using fat for energy. \n\nSugar burners are more insulin dominant compared to other types and easily become insulin resistant. This requires the body to produce more and more insulin over time. Insulin is a hormone that sends a signal to store fat and also decreases the release of fat from fat tissue. This is largely the results of its stimulatory and inhibiting impact on the enzymes lipoprotein lipase (LPL) and hormone sensitive lipase (HSL) respectively. \n\nSugar burners, those who struggle with their weight and tend to be more globally overweight, need to control carbs more than any other type. This is because carbohydrates are the major stimulator of the hormone insulin for most people, which is already an issue for sugar burners. Therefore, sugar burners should start with lower amounts of carbs. We usually keep them  to 10g to 15g of carbs at each of the three major meals. With protein and vegetables and  little to no carb at snacks. The range has to do with carb quality. The “whiter” the carb, (i.e. white rice, white pasta, white bread, sweets, cookies, etc.), the less carb they are instructed to eat. The more whole the starch, (i.e. whole grain bread, high fiber cereal, potatoes including their skins, beans, squashes, etc.), the more or higher range of starch allowed. \n\n The burner type designations are meant to help you find a good starting point from which to begin to decipher how your metabolism works. From there you measure fat loss and metabolic feedback signals (hunger, cravings, and energy) so that you get results. The power lies in this process. Your metabolism will and does change. Pregnancy, menopause, andropause, times of stress, etc., all result in metabolic changes that may or may not correct on their own. By understanding the Metabolic Effect diet process you will always have the tools to lose fat no matter what is happening with your metabolism. In other words, the burner type means nothing without the process.  In reality, if you do the program correctly, you should end up at your own unique metabolic burner type. \n\nNOTE: The Metabolic Effect fat loss lifestyle is a program that needs to be learned, practiced, and mastered. We offer a 10-week online fat loss coaching program that teaches you this entire system in a self-paced and email based format. It is like going to fat loss school.";
	} else if (tot < 35 && tot > 20) {
		head = "Mixed Burner";
		answer = "In the Metabolic Effect way of thinking, there are three burner types and they lie on a continuum. In the middle there are the mixed burners who burn a relatively even mix of sugar and fat. They have a more balanced hormonal makeup between stress hormones and fat storing hormones. \n\nMixed burners are right in the middle and can be nudged toward the sugar burner side of the equation if they overindulge in carbs and fat. At the same time, they can also be pulled to the muscle burner side if they are overexposed and/or acutely reactive to stress. Mixed burners have a much easier time making body change because they usually have athletic and muscular builds. \n\nThese types are those who will usually respond to any change in diet for a short period of time. But chronic dieting, overeating, and lack of exercise causes them to lose muscle and develop dysfunctional metabolisms. Most people, and almost all younger people, are mixed burners. This percentage changes as we age. When stress and poor eating habits catch up with people, they begin to move toward the muscle or sugar burner category. \n\nMixed burners fall in the middle with a carb intake range between 15g and 30g at each major meal. Carbohydrates for mixed burners can be looked at strictly as fuel. The tendency of mixed burners to stress more or overeat more is a good predictor of whether they should eat more or less carbs. If they are more stress driven, they may need more carbs. If they are more food driven they should eat less carbs and instead do more protein based foods. Carb timing works very well in mixed burners and is an excellent strategy for them. When after fat loss, carb intakes that are greater in the morning and post-workout do very well for mixed burners. \n\nThe burner type designations are meant to help you find a good starting point from which to begin to decipher how your metabolism works. From there you measure fat loss and metabolic feedback signals (hunger, cravings, and energy) so that you get results. The power lies in this process. Your metabolism will and does change. Pregnancy, menopause, andropause, times of stress, etc., all result in metabolic changes that may or may not correct on their own. By understanding the Metabolic Effect diet process you will always have the tools to lose fat no matter what is happening with your metabolism. In other words, the burner type means nothing without the process.  In reality, if you do the program correctly, you should end up at your own unique metabolic burner type. \n\nNOTE: The Metabolic Effect fat loss lifestyle is a program that needs to be learned, practiced, and mastered. We offer a 10-week online fat loss coaching program that teaches you this entire system in a self-paced and email based format. It is like going to fat loss school.";
	} else {
		head = "Muscle Burner";
		answer = "In the Metabolic Effect way of thinking, there are three burner types and they lie on a continuum. On the other end there are the muscle burners, who also run primarily off sugar, yet seem to derive much of this fuel by converting amino acids from muscle into sugar. This is because they are stress hormone dominant which results in conversion of their own muscle tissue into glucose for fuel. They are less reliant on food for this reason and can usually go without food without difficulty. \n\nMuscle burners also run their engines off of sugar, but they are more stress hormone dominant than insulin dominant. In others words, they have higher resting levels of the stress hormones adrenaline and cortisol. These hormones raise blood sugar by breaking down stored glycogen, but also cause the body to release amino acids from muscle tissue to make sugar in a process called gluconeogenesis (i.e. generating new glucose). \n\nMuscle burners need a little more carbs than the other types. This is often confusing for people, but remember the primary hormonal dysfunction in this group comes from stress hormones. These stress hormones are released mainly to raise blood sugar levels. By supplying more carbs to these types, we are effectively preempting this entrained stress hormone release. The best way to oppose the action of stress hormones is to throw enough glucose & insulin in the mix to control the blood sugar, but not so much to throw things further out of balance. In other words, they can’t go overboard with carbs, but also should not reduce them too low. We usually recommend these types eat between 20g to 50g of carb at each major meal. This is a very wide range of carb intake and muscle burners often have the most difficult time finding the optimal carb level and controlling this “stress response”. \n\nThe adrenal stress reactions they have can be unpredictable and influenced by many things. Therefore, muscle burners have to often work harder than any other type to balance starch and other metabolic influences. This more difficult metabolic response in muscle burners comes from the fact that stress hormones not only raise blood sugar levels, but can also disrupt insulin metabolism. When that mechanism is pushed too far, it is very difficult to tell a muscle burner from a sugar burner. Muscle burner snacks usually focus exclusively on protein and fiber, but they often need small amounts of starch at snacks as well. \n\nThe burner type designations are meant to help you find a good starting point from which to begin to decipher how your metabolism works. From there you measure fat loss and metabolic feedback signals (hunger, cravings, and energy) so that you get results. The power lies in this process. Your metabolism will and does change. Pregnancy, menopause, andropause, times of stress, etc., all result in metabolic changes that may or may not correct on their own. By understanding the Metabolic Effect diet process you will always have the tools to lose fat no matter what is happening with your metabolism. In other words, the burner type means nothing without the process.  In reality, if you do the program correctly, you should end up at your own unique metabolic burner type. \n\nNOTE: The Metabolic Effect fat loss lifestyle is a program that needs to be learned, practiced, and mastered. We offer a 10-week online fat loss coaching program that teaches you this entire system in a self-paced and email based format. It is like going to fat loss school.";

	}

	Alloy.Globals.backHistory.pop().close();

	//sends the user to a results screen
	Alloy.createController("survey_results", {
		head : head,
		message : answer,
	}).getView().open();
};

init();
