var args = arguments[0] || {};
var moment = require('alloy/moment');

var upperCase = function(str) {
	if(str) {
		return str.toUpperCase();
	}
};

if(args.image.charAt(0) != 'h'){
	args.image = "/images/blogPlaceHolder.png";
}
$.articleImage.image = args.image;
$.articleDate.text = moment(args.published).format("MMMM Do, YYYY");
$.articleTitle.text = args.title;
$.articleCategory.text = upperCase(args.category);

Ti.API.info(args.title);


$.articleRow.addEventListener('click', function(){
	Alloy.createController('news_detail',
	{
		image: args.image,
		date: $.articleDate.text,
		id: args.id,
		title: args.title,
		category : args.category,
		link: args.link
	}).getView().open();
});

/*
 * Rezise images
 */
(function(){
	var image = $.articleImage.toImage();
	if(image.width >= image.height){
		var ratio = image.height / 98;
		$.articleImage.width = image.width / ratio + "dp";
		$.articleImage.height = image.height / ratio + "dp";
	} else {
		var ratio = image.width / 98;
		$.articleImage.width = image.width / ratio + "dp";
		$.articleImage.height = image.height / ratio + "dp";
	}
})();
