var args = arguments[0] || {};

$.name.text = args.name;
$.row.filter = args.name;

$.row.addEventListener("click", function(e) {
	Alloy.createController("dr_results", {
		specialityId: args.specialityId,
		stateId: args.stateId,
		cityId: args.cityId
	}).getView().open();
});