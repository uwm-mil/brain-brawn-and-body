//Start and Initialize Moment
Ti.API.info("===CREATING JOURNAL ROW===");
var moment = require('alloy/moment');

//Get and Inspect Arguments
var args = arguments[0] || {};

Ti.API.info("Inspecting Object: args:" + args);
for (var thing in args) {
  Ti.API.info("args." + thing + " = " + args[thing]);
}

//Variables
var type = args.type;
var preDate = args.date;
Ti.API.info("DATE IN THE ROW : " + moment(args.date, "YYYYMMDD").format("MM d"));

//Set Label Contents
$.entryTitle.text = args.title;
$.entryContent.text = args.text;
$.entryDate.text = moment(args.date, "YYYYMMDD").format('D');
$.entryDay.text = moment(args.date, "YYYYMMDD").format('ddd').toUpperCase();
$.entryMonth.text = moment(args.date, "YYYYMMDD").format("MMM").toUpperCase();
$.entryDateWrapper.backgroundColor = args.color;

//We're Done Here
Ti.API.info("===DONE CREATING JOURNAL ROW===");

//Listener for row editing or deleting
$.journalRow.addEventListener('click', function(e){
	//bubble to find parent TableViewRow
	var row = e.source;
	if(row != "[object TiUIButton]"){
		
		//create and display journal_edit controller
 	   Alloy.createController('journal_edit',{
 	   		id : args.id,
 	   		typeColor : args.color,
 	 		title : args.title,
			date :  args.date,
			content : args.text
		}).getView().open();
	}
});