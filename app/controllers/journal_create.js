var type = "notSelected";
var Cloud = require('ti.cloud');
var picker;
var dateToExport;
var animation = require('alloy/animation');
var moment = require('alloy/moment');
var args = arguments[0] || {};

//add this controller to the back history
Alloy.Globals.backHistory.push($.journal_create);

$.medicalCategoryBtn.opacity = "0.5";
$.milestonesCategoryBtn.opacity = "0.5";
$.notesCategoryBtn.opacity = "0.5";

// set blanks based on passed in values
if (args.screening && args.dateCompleted) {
	$.entryContent.setValue(("Today I got my " + args.screening + " completed."));
	$.entryTitle.setValue(args.screening);
	$.entryDate.text = args.dateCompleted.format('MMMM Do YYYY');
	dateToExport = args.dateCompleted.format('YYYYMMDD');
}
else if(args.content && args.dateCompleted) {
	$.entryContent.setValue(args.content);
	$.entryTitle.setValue(args.screeningTitle);
	$.entryDate.text = args.dateCompleted.format('MMMM Do YYYY');
	dateToExport = args.dateCompleted.format('YYYYMMDD');
}

var currentTime = new Date();
var year = currentTime.getFullYear();

var pickerView = Ti.UI.createView({
	width : Ti.UI.FILL,
	height : Ti.UI.SIZE,
	layout : "vertical"
});

var picker = Ti.UI.createPicker({
	type : Ti.UI.PICKER_TYPE_DATE,
	bottom : "0dp",
	minDate : new Date(year - 5, 0, 1),
	maxDate : new Date(year + 1, 11, 31),
	value : new Date(),
	height : Ti.UI.SIZE
});
var button = Ti.UI.createButton({
	title : "Set Date",
	height : "32dp",
	width : Ti.UI.FILL,
	top : "4dp",
	right : "4dp",
	bottom : "4dp",
	left : "4dp",
	style : "plain",
	backgroundColor : "#EE5733",
	color : "#fff",
	font : {
		fontSize : "12dp",
		fontWeight : "bold"
	}
});
pickerView.add(button);
pickerView.add(picker);
$.pickerWrapper.add(pickerView);
button.addEventListener("click", function() {
	// set value
	var pickerdate = picker.value;
	var day = pickerdate.getDate();
	var month = pickerdate.getMonth() + 1;
	if (month < 10)
		month = "0" + month;
	var year = pickerdate.getFullYear();
	var newdate = year + "-" + month + "-" + day;
	$.entryDate.text = moment(newdate).format('MMMM Do YYYY');
	dateToExport = moment(newdate).format('YYYYMMDD');
	// animate off
	$.pickerWrapper.animate({
		bottom : "-260dp",
		duration : 250,
		curve : Titanium.UI.ANIMATION_CURVE_EASE_IN_OUT
	});
});

$.entryDateWrapper.addEventListener('click', function(e) {
	//Hide the Keyboard
	// scroll height after adding picker
	$.pickerWrapper.bottom = "-260dp";
	$.pickerWrapper.animate({
		bottom : "0dp",
		duration : 250,
		curve : Titanium.UI.ANIMATION_CURVE_EASE_IN_OUT
	});
});
// picker.addEventListener('change',function(e){
//   Ti.API.info("User selected date: " + e.value.toLocaleString().replace(", 12:00:00 AM CDT", ""));
// });

/***********************/

$.entryCategory.addEventListener('click', function(e) {
	//turn all buttons off
	for (var i = 0; i < this.children.length; i++) {
		this.children[i].children[0].opacity = "0.5";
	}
	//turn clicked button on
	e.source.opacity = "1.0";
});

//Listeners to assign journal type
$.medicalCategoryBtn.addEventListener('click', function(e) {
	Ti.API.info("Medical Note");
	type = "medical";
});

$.milestonesCategoryBtn.addEventListener('click', function(e) {
	Ti.API.info("Milestones Note");
	type = "milestones";
});

$.notesCategoryBtn.addEventListener('click', function(e) {
	Ti.API.info("General Note");
	type = "notes";
});

//creates a new journal entry
$.submitEntryBtn.addEventListener('click', function(e) {

	if (type !== "notSelected" && $.entryDate.text !== "Select a date") {
		Cloud.Objects.create({
			classname : 'journals',
			fields : {
				type : type,
				text : $.entryContent.value,
				// Date Format: YYYYMMDD,
				date : dateToExport,
				title : $.entryTitle.value
			}
		}, function(e) {
			if (e.success) {
				Ti.API.info("Journal Entered");
				if(args.content) {
					Alloy.createController('screenings').getView().open();
				}
				else {
					Alloy.Globals.backHistory.pop().close();
				}
				Titanium.App.fireEvent('journalCreated');
			} else {
				alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
			}
		});
	} else {
		alert("Please fill out all of the options.");
	}
});