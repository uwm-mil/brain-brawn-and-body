/*
* set categories view height
*/
//how many categories should show on pop up
//var rowsToShow = 4.5;
//multiply number of rows by row height then add button height
//var height = rowsToShow * 48 + 32;

//set category wrapper to the size of it contents
$.blogCategoriesWrapper.setHeight("248dp");

//hide below the screen minus the height of the button so the button shows
$.blogCategoriesWrapper.setBottom("-216dp");

/*	*	*	*	*	*	*	*	*	*	*	*/
var filter = "all";
var args = arguments[0] || {};
var loading = Alloy.createWidget("com.mcongrove.loading").getView();

var formatDate = function() {
	var date = new Date();
	// var datestr = d.getMonth()+'/'+d.getDate()+'/'+d.getFullYear();
	var datestr = "";
	var min = date.getMinutes();
	if (date.getMinutes() < 10)
		min = "0" + date.getMinutes();
	if (date.getHours() >= 12) {

		datestr += ' ' + (date.getHours() == 12 ? date.getHours() : date.getHours() - 12) + ':' + min + ' PM';
	} else {
		datestr += ' ' + date.getHours() + ':' + min + ' AM';
	}
	return datestr;
};

var removeHTMLEntities = function(text) {
	var newText = text.replace("&#147;", "\"");
	var newText2 = newText.replace("&#148;", "\"");
	var newText3 = newText2.replace("&#146;", "\'");
	return newText3;
};

//Storing the articles to be filtered
var storedArticles = [];
var getArticles = function(u) {
	$.parentWindow.add(loading);
	var data = [];
	var xhr = Ti.Network.createHTTPClient();
	xhr.onload = function(e) {
		try {
			//Ti.API.info(this.responseText);
			var arr = JSON.parse(this.responseText);
			for (var i = 0; i < arr.length; i++) {
				var entry = arr[i];
				var row = Alloy.createController("news_row", {
					title : removeHTMLEntities(entry.title),
					id : entry.id,
					published : entry.published,
					image : entry.image,
					category : entry.category,
					link : entry.url
				}).getView();

				data.push(row);
				storedArticles.push(entry);
			}
			$.mainContent.setData(data);
			$.parentWindow.remove(loading);
		} catch(e) {
			Ti.API.info(e);
		}
	};
	xhr.open("GET", u);
	xhr.send();
};

/*
 * show category menu
 */
var newsCategoryMenuIsShowing = false;
$.toggleBlogCategoriesBtn.addEventListener('click', function() {
	if (newsCategoryMenuIsShowing) {
		newsCategoryMenuIsShowing = false;
		$.blogCategoriesWrapper.animate({
			bottom : "-216dp",
			duration : 250,
			curve : Titanium.UI.ANIMATION_CURVE_EASE_IN_OUT
		});
	} else {
		newsCategoryMenuIsShowing = true;
		$.blogCategoriesWrapper.animate({
			bottom : "0dp",
			duration : 250,
			curve : Titanium.UI.ANIMATION_CURVE_EASE_IN_OUT
		});
	}
});

var filterContent = function(rowId) {
	$.parentWindow.add(loading);
	filter = rowId;
	if (rowId === "blogs") {
		getArticles("http://www.brainbrawnbody.com/api/articles.php?key=1ztJuBFSaUR9FXieDCIv&cat=blogs");
		if (newsCategoryMenuIsShowing) {
			$.toggleBlogCategoriesBtn.fireEvent("click");
		}
	} else {
		var articlesShown = [];
		var filteredRows = [];
		if (rowId === "all") {
			articlesShown = storedArticles;
		} else {
			for (var i = 0; i < storedArticles.length; i++) {
				if (storedArticles[i].category === rowId) {
					articlesShown.push(storedArticles[i]);
				}
			}
		}
		for (var j = 0; j < articlesShown.length; j++) {

			var row = Alloy.createController("news_row", {
				title : removeHTMLEntities(articlesShown[j].title),
				id : articlesShown[j].id,
				published : articlesShown[j].published,
				image : articlesShown[j].image,
				category : articlesShown[j].category
			}).getView();
			filteredRows.push(row);
		}
		$.mainContent.setData(filteredRows);
		if (newsCategoryMenuIsShowing) {
			$.toggleBlogCategoriesBtn.fireEvent("click");
		}
	}
	$.parentWindow.remove(loading);
};

//Shows filtered content without making additional api calls
$.blogCategories.addEventListener('click', function(e) {
	if (e.rowData.id != "refresh") {
		Ti.API.info(e.rowData.id);
		filterContent(e.rowData.id);
	} else {
		getArticles("http://www.brainbrawnbody.com/api/articles.php?key=1ztJuBFSaUR9FXieDCIv");
		if (newsCategoryMenuIsShowing) {
			$.toggleBlogCategoriesBtn.fireEvent("click");
		}
	}
});

getArticles("http://www.brainbrawnbody.com/api/articles.php?key=1ztJuBFSaUR9FXieDCIv"); 