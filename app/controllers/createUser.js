var Cloud = require('ti.cloud');
Alloy.Globals.backHistory.push($.win);

var newsletter = false;

$.newsletterCheckbox.addEventListener('click', function() {
	if (newsletter) {
		this.backgroundColor = "#ffffff";
		newsletter = false;
	} else {
		this.backgroundColor = "#ee5733";
		newsletter = true;
	}
});

$.registerButton.addEventListener("click", function(e) {
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	if (reg.test($.username.value) == false) {
		alert("The e-mail address you entered is invalid.");
	} else if ($.username.value == "" || $.firstName.value == "" || $.lastName.value == "" || $.password.value == "" || $.passwordConfirm.value == "") {
		alert("You didn't fill in all of the boxes. All boxes are required.");
	} else if ($.password.value !== $.passwordConfirm.value) {
		alert("The password and password confirmation don't match. Please correct and try again.");
	} else {
		Cloud.Users.create({
			email : $.username.value.toLowerCase(),
			first_name : $.firstName.value,
			last_name : $.lastName.value,
			password : $.password.value,
			password_confirmation : $.passwordConfirm.value
		}, function(e) {
			if (e.success) {
				if (newsletter) {
					Ti.API.info("Sending Information to Subscription API");
					var xhr = Ti.Network.createHTTPClient();
					xhr.onload = function(e) {
						Ti.API.info("Successfully Sent Information to API");
						//Ti.API.info("Repsonse from API: " + e.responseText);
					};
					xhr.open('POST', 'http://www.brainbrawnbody.com/api/subscribe.php?key=1ztJuBFSaUR9FXieDCIv');
					xhr.send({
						name : $.firstName.value + " " + $.lastName.value,
						email : $.username.value.toLowerCase()
					});
				}
				var user = e.users[0];
				Ti.App.Properties.setString("logUser", user.id);
				Ti.API.info('Cloud Register Success:\n' + 'id: ' + user.id + '\n' + 'sessionId: ' + Cloud.sessionId + '\n' + 'first name: ' + user.first_name + '\n' + 'last name: ' + user.last_name);
				alert("Thank you for registering with us " + $.firstName.value + " " + $.lastName.value + "! You may now login with your email address and password.");
				$.win.close();
			} else {
				alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
			}
		});
	}
}); 

$.newsletterCheckbox.fireEvent("click");
