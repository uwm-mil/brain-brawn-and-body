//add this controller to the back history
Alloy.Globals.backHistory.push($.news_detail);
var ezsocial = require('ezsocial');
var args = arguments[0] || {};
Ti.API.info("Inspecting Object: args:" + args);
for (var thing in args) {
  Ti.API.info("args." + thing + " = " + args[thing]);
}

$.articleImage.image = args.image;
$.articleTitle.text = args.title;
$.articleDate.text = args.date;
$.articleCategory.text = args.category.toUpperCase();

//apply image
var listener = function(){
	var image = $.articleImage.getRect();	
	var height = image.height * $.contentWrapper.getRect().width / image.width;
	
	$.articleImage.setWidth($.contentWrapper.getRect().width);
	$.articleImage.setHeight(height);
	
	//show image, hidden to hide resizing
	//$.articleImage.opacity = 1;
	
	$.articleImage.animate(Titanium.UI.createAnimation({
	    curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT,
	    opacity: 1,
	    duration: 500
	}));
	
	$.contentWrapper.removeEventListener('postlayout', listener);
};

$.contentWrapper.addEventListener('postlayout', listener);


var getArticleText = function(u) {
	var data = [];
	var xhr = Ti.Network.createHTTPClient();
	xhr.onload = function(e) {
		try {
			var content = this.responseText;
			$.articleContent.text = content;
			$.articleContent.setHeight(Ti.UI.SIZE);
			$.articleContent.animate(Titanium.UI.createAnimation({
			    curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT,
			    opacity: 1,
			    duration: 250
			}));
		} catch(e) {
			Ti.API.info(e);
		}
	};
	xhr.open("GET", u);
	xhr.send();
};



if($.articleCategory.text.indexOf("BLOG") > -1) {
	getArticleText("http://www.brainbrawnbody.com/api/post.php?key=1ztJuBFSaUR9FXieDCIv&id=" + args.id);
}
else {
	getArticleText("http://www.brainbrawnbody.com/api/read.php?key=1ztJuBFSaUR9FXieDCIv&id=" + args.id);
}

$.shareButton1.addEventListener("click", function(e) {
	ezsocial.share(args.title, args.link);
});

$.shareButton2.addEventListener("click", function(e) {
	$.shareButton1.fireEvent("click");
});
