//add this controller to the back history
Alloy.Globals.backHistory.push($.dr_cities);

var args = arguments[0] || {};

/*
var fsCache = require('fscache');

var listCities = function(arr) {
	var data = [];
	for (var i = 0; i < arr.result.length; i++) {
		var city = arr.result[i];
		Ti.API.info("Inspecting Object: city:" + city);
		for (var thing in city) {
		  Ti.API.info("city." + thing + " = " + city[thing]);
		}
		data.push(Alloy.createController("dr_cities_row", {
			cityId : city.id,
			stateId: args.stateID,
			name : city.name,
			stateName: args.name
		}).getView());
	}
	$.tv.setData(data);
};

fsCache.process("https://api.doctoralia.com/v1/US/provinces/" + args.stateID + "/cities?apiKey=8da25879", 86400, "cities_" + args.stateID, listCities);
*/

//$.tv.search = $.tableSearch;

var url = "https://api.doctoralia.com/v1/US/provinces/" + args.stateID + "/cities?apiKey=8da25879";
var client = Ti.Network.createHTTPClient({
		onload : function(e) {			
			var result = JSON.parse(this.responseText).result;
			var data = [];
			for (var i = 0; i < result.length; i++) {
				var city = result[i];
				data.push(Alloy.createController("dr_cities_row", {
					cityId : city.id,
					stateId: args.stateID,
					name : city.name,
					stateName: args.name
				}).getView());
			}
			$.tv.setData(data);
			
		},
		onerror : function(e) {
			Ti.API.debug(e.error);
			alert('error');
		},
		timeout : 5000  // in milliseconds
	});
client.open("GET", url);
client.send();