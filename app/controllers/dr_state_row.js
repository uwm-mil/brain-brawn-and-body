var args = arguments[0] || {};

$.name.text = args.name;
$.row.filter = args.name;

switch (args.name){
	case 'Hawai HI': {
		$.name.text = "Hawaii HI";
		$.row.filter = "Hawaii HI";
		break;
	}
	case 'Luisiana LA': {
		$.name.text = "Louisiana LA";
		$.row.filter = "Louisiana LA";
		break;
	}
}

$.row.addEventListener("click", function(e) {
	 Alloy.createController("dr_cities", {
	 	stateID: args.id,
	 	name: args.name
	 }).getView().open();
});
