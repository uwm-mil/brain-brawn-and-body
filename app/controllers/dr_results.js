//add this controller to the back history
Alloy.Globals.backHistory.push($.dr_results);

var args = arguments[0] || {};

/*
var fsCache = require('fscache');

var listDoctors = function(arr) {
	var data = [];
	if(arr.result.numEntities == 0) {
		alert("There are no doctors available for this speciality in this city.");
	}
	for (var i = 0; i < arr.result.entities.length; i++) {
		var doc = arr.result.entities[i];
		Ti.API.info("Inspecting Object: doc:" + doc);
		for (var thing in doc) {
		  Ti.API.info("doc." + thing + " = " + doc[thing]);
		}
		
		data.push(Alloy.createController("dr_results_row", {
			name : doc.name,
			id: doc.id
		}).getView());
		
	}
	$.tv.setData(data);
};

fsCache.process("https://api.doctoralia.com/v1/US/professionals?specialityId=" + args.specialityId + "&cityId=" + args.cityId + "&apiKey=8da25879", 86400, "drs_s" + args.specialityId + "_c" + args.cityId, listDoctors);
*/

var url = "https://api.doctoralia.com/v1/US/professionals?specialityId=" + args.specialityId + "&cityId=" + args.cityId + "&apiKey=8da25879";
var client = Ti.Network.createHTTPClient({
		onload : function(e) {			
			var result = JSON.parse(this.responseText).result;
			var data = [];
			if(result.numEntities == 0) {
				alert("There are no doctors available for this speciality in this city.");
			}
			for (var i = 0; i < result.entities.length; i++) {
				var doc = result.entities[i];
				
				data.push(Alloy.createController("dr_results_row", {
					name : doc.name,
					id: doc.id
				}).getView());
			}

			$.tv.setData(data);
			
		},
		onerror : function(e) {
			Ti.API.debug(e.error);
			alert('error');
		},
		timeout : 5000  // in milliseconds
	});
client.open("GET", url);
client.send();

/*
 * set all buttons to 0.5 opacity
 * set first button to 1.0 opacity
 */
(function(){
	lowerButtonOpacity();
	$.buttonWrapper.children[0].children[0].opacity = "1.0";
})();



/*
 * scroll to window
 */
$.buttonWrapper.addEventListener('click', function(e){
	$.scrollableView.scrollToView(parseInt(e.source.index));
	updateButtonOpacity(e);
});



/*
 * update nav when scrollableView is swiped to a different window
 */
$.scrollableView.addEventListener('scrollEnd', function(e){
	if(e.source.id === "scrollableView"){
		lowerButtonOpacity();
		$.buttonWrapper.children[e.currentPage].children[0].opacity = "1";
	} else {
		return;
	}
});



/*
 * update button opacity
 */
function updateButtonOpacity(e){
	lowerButtonOpacity();
	e.source.opacity = "1";
}



/*
 * set button opacity to 0.5
 */
function lowerButtonOpacity(){
	for(var i = 0; i < $.buttonWrapper.children.length; i++){
		$.buttonWrapper.children[i].children[0].opacity = "0.5";
	}	
}