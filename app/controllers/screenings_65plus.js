var Cloud = require('ti.cloud');
var userLog = Ti.App.Properties.getString("userLog");
var entries = [];
var completedEntries = [];
var rows = [];
var currentRow = -1; // -1 is a non-selected state
var entriesLoaded = false;
var completedFound = false;
var setCompletedDone = false;
var clearDone = false;
var errorFlag = false;
var createFlag = false;
var moment = require('alloy/moment');

///////////

var incompleteDialog = Ti.UI.createAlertDialog({
	markAndAdd: 0,
	neither: 1,
	buttonNames: ['Yes','No'],
	message: 'Would you like to mark this screening as complete and add it to your journal?',
	title: 'Screening Options'
});

var completeDialog = Ti.UI.createAlertDialog({
	reset: 0,
	clear: 1,
	ok: 2,
	buttonNames: ['Reset Date','Clear','Cancel'],
	message: 'Your last screening was completed on 01-01-0101.\nYou\'re due for your next screening 1010 days from now.',
	title: 'Screening Options'
});


var getIndex = function(rootObj, childObj){
	var index = null;	
	var bubbleMachine = function(rootObj, childObj){
		if(childObj === rootObj){
			//do nothing, child is parent
		} else if(childObj.index){
			//found index, return
			index = childObj.index;
		} else {
			//no index, check parent
			bubbleMachine(rootObj, childObj.parent);
		}
	};
	bubbleMachine(rootObj, childObj);
	
	return index;
};

//Retrieves screenings from ACS
var getEntries = function() {
	entries = [];
	rows = [];
	Cloud.Objects.query({
		classname : 'screening',
		page: 1,
		per_page : 100,
		order : "name"
	}, function(e) {
		if (e.success) {
			entriesLoaded = false;
			var num = 1;
			for (var i = 0; i < e.screening.length; ++i) {

				if(e.screening[i].ageGroup == 4){
					// store for later
					entries.push(e.screening[i]);

					// create views
					var screeningRow = Ti.UI.createView({
						top : "2dp",
						height : "40dp",
						index : num
					});
					var screeningData = Ti.UI.createView({
						layout: "horizontal",
						top: "4dp",
						right: "8dp",
						bottom: "4dp",
						left: "8dp"
					});
					var screeningIntervalWrapper = Ti.UI.createView({
						backgroundColor : "#aa7da1",
						width : "72dp",
						height : "32dp"
					});
					//
					var screeningInterval = Ti.UI.createLabel({
						width: "30dp",
						top: "5dp",
						left: "0dp",
						height: "32dp",
						color: "#3c2316",
						textAlign: Ti.UI.TEXT_ALIGNMENT_RIGHT,
						font:{
							fontSize: "24dp",
							fontFamily: "HelveticaNeueLTStd-Lt"
						},
						text : e.screening[i].interval
					});
					var screeningYearText = Ti.UI.createLabel({
						width: "40dp",
						height: "11dp",
						top: "5dp",
						left: "32dp",
						color: "#3c2316",
						font:{
							fontSize: "10dp"
						},
						text : "Year"
					});
					var screeningIntervalText = Ti.UI.createLabel({
						width: "40dp",
						height: "11dp",
						bottom: "5dp",
						left: "32dp",
						color: "#3c2316",
						font:{
							fontSize: "10dp"
						},
						text : "Interval"
					});
					//
					var screeningTypeWrapper = Ti.UI.createView({
						width: Ti.UI.FILL,
						height: Ti.UI.FILL,
						backgroundColor: "#fff"
					});
					var screeningType = Ti.UI.createLabel({
						left: "4dp",
						color: "#3c2316",
						font:{
							fontSize: "18dp",
							fontFamily: "HelveticaNeueLTStd-Lt"
						},
						text : e.screening[i].name
					});
					var completedLayer = Ti.UI.createView({
						top: "4dp",
						right: "8dp",
						bottom: "4dp",
						left: "8dp",
						backgroundColor : "gray",
						opacity : 0.5
					});

					// add the elements into eachother and make the complete "row"
					screeningIntervalWrapper.add(screeningInterval);
					screeningIntervalWrapper.add(screeningYearText);
					screeningIntervalWrapper.add(screeningIntervalText);

					screeningTypeWrapper.add(screeningType);

					screeningData.add(screeningIntervalWrapper);
					screeningData.add(screeningTypeWrapper);

					screeningRow.add(screeningData);
					screeningRow.add(completedLayer);

					// add view to scrollview
					$.scrollView.add(screeningRow);
					rows.push(screeningRow);
					++num;
				}
			}
			entriesLoaded = true;
			Ti.API.info("entriesloaded");
		}
		else {
			// alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
			errorFlag = true;
		}
	});
};

var getCompleted = function(){
	// test for logged in and check completed
	completedEntries = [];
	completedFound = false;

	if (Ti.App.Properties.getString("userLog") != null) {
		Ti.API.info("Logged in");

		Cloud.Objects.query({
			classname : 'screenings',
			where : {
				user_id : userLog
			}
		}, function(e) {
			if (e.success) {
				for (var i = 0; i < e.screenings.length; ++i) {
					if(e.screenings[i].ageGroup == 4){
						// store for later
						completedEntries.push(e.screenings[i]);
					}
				}
				completedFound = true;
				Ti.API.info("completedfound");
			} else {
				// alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
				errorFlag = true;
			}
		});

	}
};

var clearCompleted = function(){
	clearDone = false;
	if(entriesLoaded == true){
		for(var i = 0; i < rows.length; ++i){
			rows[i].children[1].setVisible(false);
		}
		clearDone = true;
		Ti.API.info("cleardone");
		clearInterval(clearCompletedInterval);
	}
};
var setCompleted = function(){
	setCompletedDone = false;
	if(entriesLoaded == true && completedFound == true){
		for(var i = 0; i < completedEntries.length; ++i){
			for(var j = 0; j < entries.length; ++j){
				if(completedEntries[i].name == entries[j].name){
					entries[j].complete = true;
					entries[j].dateCompleted = completedEntries[i].dateCompleted;
				}
			}	
		}
		setCompletedDone = true;
		Ti.API.info("setcompletedone " + completedEntries.length);
		clearInterval(setCompletedInterval);
	}
};
var markCompleted = function(){
	if(completedFound == true && clearDone == true && entriesLoaded == true && setCompletedDone == true){
		// find the completed one from completedEntries[] in entries[] and update
		// it like this ->rows[i].children[1].setVisible(true);
		for(var i = 0; i < entries.length; ++i){
			if(entries[i].complete == true){
				rows[i].children[1].setVisible(true);
			}
		}
		Ti.API.info("markcompletedone");
		clearInterval(markCompletedInterval);
	}
};

$.scrollView.addEventListener('click', function(e) {
	if (Ti.App.Properties.getString("userLog") != null) {
		currentRow = getIndex(this, e.source) - 1;
		// test if valid row
		if (currentRow != null && currentRow >= 0) {
			Ti.API.info("Inspecting Object: " + entries[currentRow]);
			for (var thing in entries[currentRow]) {
				Ti.API.info("entries[currentRow]." + thing + " = " + entries[currentRow][thing]);
			}
			Ti.API.info("Row " + currentRow + " is complete : " + entries[currentRow].complete);
			if (entries[currentRow].complete) {
				// update message for complete dialog
				var tempDate = moment(entries[currentRow].dateCompleted);
				var firstHalf = "Your last screening was completed on " + moment(entries[currentRow].dateCompleted).format('MMMM Do, YYYY');
				var adjustedYear = tempDate.add("y", entries[currentRow].interval);
				adjustedYear = adjustedYear.format('MMMM Do, YYYY');
				var secondHalf = ".\nYou\'re due for your next screening " + adjustedYear;
				completeDialog.message = firstHalf + secondHalf;
				// show alert
				completeDialog.show();
			} else {
				incompleteDialog.show();
			}
		}
	} else {
		alert("You must be logged in before you can mark your screenings as completed");
	}
});

incompleteDialog.addEventListener('click', function(e) {
	if (e.index === e.source.neither) {
		Ti.API.info('The neither button was clicked');
	} else if (e.index === e.source.markAndAdd) {
		Ti.API.info('The markAndAdd button was clicked');
		if (entries[currentRow].requires_data == true) {
			Ti.API.info("This screening DOES require data entry");
			Alloy.createController('screenings_entry', {
				ageGroup: 4,
				screening : entries[currentRow].name,
				id : entries[currentRow].id
			}).getView().open();
		} else {
			// mark it in Entries as completed
			entries[currentRow].complete = true;
			entries[currentRow].dateCompleted = moment();
			rows[currentRow].children[1].setVisible(true);

			// create the cloud object
			Cloud.Objects.create({
				classname : 'screenings',
				fields : {
					ageGroup : 4,
					name : entries[currentRow].name,
					dateCompleted : moment()
				}
			}, function(e) {
				if (e.success) {
					Ti.API.info("Completed screening created");
					// refresh the Completed Entries
					getCompleted();
				} else {
					// alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
					errorFlag = true;
				}
			});

			// open a create journal view controller with row's data
			if (userLog) {
				Alloy.createController('journal_create', {
					screening : entries[currentRow].name,
					dateCompleted : entries[currentRow].dateCompleted
				}).getView().open();
			} else
				alert("You're not currently logged in under the Your Health tab.\nPlease log in to be able to use the journal.");
		}
	}
	currentRow = -1;
});

completeDialog.addEventListener('click', function(e){
	if (e.index === e.source.reset){
		Ti.API.info('The reset date button was clicked');
		// mark screening as incomplete:

		// set it in completed entries
		for(var i = 0; i < completedEntries.length; ++i){
			if(completedEntries[i].name == entries[currentRow].name){
				completedEntries[i].dateCompleted = moment();

				// set it in cloud
				Cloud.Objects.update({
					classname : 'screenings',
					id : completedEntries[i].id,
					fields : {
						dateCompleted : moment()
					}
				}, function(e) {
					if (e.success) {
						Ti.API.info("Completed Screenings Updated");
						// refresh the Completed Entries
						getCompleted();
					} else {
						alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
					}
				});
			}			
		}
		// set it in entries
		entries[currentRow].dateCompleted = moment();
	}
	else if (e.index === e.source.clear){
		Ti.API.info('The clear button was clicked');
		// delete it from completed entries
		var tempArray = [];

		for(var i = 0; i < completedEntries.length; ++i){
			if(completedEntries[i].name == entries[currentRow].name){
				// clear it from cloud
				Ti.API.info("Found it");
				Cloud.Objects.remove({
					classname : 'screenings',
					id : completedEntries[i].id
				}, function(e) {
					if (e.success) {
						Ti.API.info("Completed Screenings Removed");
					} else {
						alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
					}
				});
			}
			else{
				tempArray.push(completedEntries[i]);
			}
		}
		completedEntries = tempArray;

		// set it in entries
		entries[currentRow].complete = false;
		rows[currentRow].children[1].setVisible(false);
	}
	else if (e.index === e.source.ok){
		Ti.API.info('The ok button was clicked');
	}
	currentRow = -1;
});

//////////////

getEntries();
getCompleted();
var clearCompletedInterval = setInterval(clearCompleted,250);
var setCompletedInterval = setInterval(setCompleted,250);
var markCompletedInterval = setInterval(markCompleted,250);
setTimeout(function(){
	if(errorFlag){
		entriesLoaded = completedFound = setCompletedDone = clearDone = false;
		clearInterval(markCompletedInterval);
		clearInterval(setCompletedInterval);
		clearInterval(clearCompletedInterval);
		alert("Unable to fetch screenings at this time. Please try again.");
	}
},5000);
