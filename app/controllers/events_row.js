var args = arguments[0] || {};

$.eventRow.addEventListener('click', function(e){
		
	//create and display journal_edit controller
 	Alloy.createController('event_details',{
   		weekday : args.weekday,
		month : args.month,
		date :  args.date,

		startTime :  args.startTime,
		endTime :  args.endTime,

		title :  args.title,
		detail :  args.detail,
		//
		tickets : args.tickets,
		location : args.location,
		id: args.id
	}).getView().open();
});

$.weekdayLabel.text = args.weekday;
$.monthLabel.text = args.month;
$.dateLabel.text = args.date;

$.startTimeLabel.text = args.startTime;
if($.startTimeLabel.text === "Event time"){
	$.startTimeLabel.opacity = ".5";
}
$.endTimeLabel.text = args.endTime;
if($.endTimeLabel.text === "not given"){
	$.endTimeLabel.opacity = ".5";
}

$.titleLabel.text = args.title;

$.detailLabel.text = args.detail;